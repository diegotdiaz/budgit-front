export default function testData() {
   return [{
       id : "UDNK3N2K3JNN34JNF",
       name: "Main",
       amount: 250,
       balance: 180,
       transactions : [
           {
               id: "1",
               label : "InnoGames GmbH",
               date : "Apr 1",
               amount : 2000
           },
           {
               id: "2",
               label : "Rewe Grocery",
               date : "Apr 7",
               amount : 10.50
           },
           {
               id: "3",
               label : "Savoy kino",
               date : "Apr 9",
               amount : 15.90
           }
       ]
   }]
}