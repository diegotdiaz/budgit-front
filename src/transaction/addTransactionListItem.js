import React from "react";
import {Avatar, Divider, ListItem, ListItemAvatar, ListItemText} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import TransactionDialog from "./transactionDialog";


export default function AddTransactionListItem() {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <div>
            <TransactionDialog open={open} handleClose={handleClose} />
            <ListItem button onClick={handleClickOpen}>
                <ListItemAvatar>
                    <Avatar>
                        <AddIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Add new Transaction" />
            </ListItem>
            <Divider/>
        </div>
    );
}