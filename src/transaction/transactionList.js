import React from "react";
import { List } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Transaction from "./transaction";
import AddTransactionListItem from "./addTransactionListItem";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function TransactionList(props) {
    const styles = useStyles();
    const transactions = props.transactions.map( (transaction) =>
         <Transaction key={transaction.id} transaction={transaction}/>
    );

    return (
      <List className={styles.root}>
          <AddTransactionListItem/>
          {transactions}
      </List>
    );
}