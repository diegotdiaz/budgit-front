import React from "react";
import {ListItem, ListItemAvatar, Avatar, ListItemText, Divider} from "@material-ui/core";
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import { makeStyles } from '@material-ui/core/styles';
import { deepOrange, deepPurple } from '@material-ui/core/colors';
import  "../helpers.js"
import "../css/transaction.css"
import {euroFormatter} from "../helpers";

const useStyles = makeStyles((theme) => ({
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    },
}));

export default function Transaction(props) {
    const classes = useStyles();
    const transaction = props.transaction;
    return (
        <div>
            <ListItem>
                <ListItemAvatar>
                    <Avatar className={classes.orange}>
                        {transaction.amount > 0 ? <AttachMoneyIcon /> : <MoneyOffIcon/>}
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={transaction.label}
                    secondary={transaction.date}
                />
                <ListItemText
                    primary={euroFormatter.format(transaction.amount)}
                />
            </ListItem>
            <Divider/>
        </div>
    );
}