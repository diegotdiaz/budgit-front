import {ADD_BUDGET, ADD_TRANSACTION} from "./actionTypes";

const initialState = {
    budgets: []
};

function rootReducer(state =  initialState, action) {
    switch (action.type) {
        case ADD_BUDGET:
            return addBudget(state, action);
        case ADD_TRANSACTION:
            return addBudgetTransaction(state, action);
        default:
            return state;
    }
}

function addBudget(state, action) {
    let budgets = state.budgets.slice().concat(action.payload);
    return Object.assign({}, state, {
        budgets: budgets
    });
}

function addBudgetTransaction(state, action) {
    let transaction = action.payload;
    let budgets = state.budgets.slice();
    for (let budget of budgets) {
        if (budget.id === transaction.budgetId) {
            budget.transactions.push(transaction);
        }
    }
    return Object.assign({}, state, {
        budgets: budgets
    });
}

export default rootReducer;