import {ADD_BUDGET, ADD_TRANSACTION} from "./actionTypes";

export function addTransaction(payload) {
    return { type : ADD_TRANSACTION, payload}
}

export function addBudget(payload) {
    return { type : ADD_BUDGET, payload}
}