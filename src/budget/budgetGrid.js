import React from "react";
import Budget from "./budget";

class BudgetGrid extends React.Component {
    render() {
        const budgetList =  this.props.budgets.map(budget => <Budget key={budget.id} budget={budget} />);
        return (
            <div id="budget_grid_container">
                <div className="budget-grid">
                    {budgetList}{budgetList}
                </div>
            </div>);

    }
}

export default  BudgetGrid;