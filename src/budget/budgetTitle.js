import React from "react";
import "../css/budget.css"

export default function BudgetTitle(props) {
    return (
      <div>
          <span>
              <h2 className="budget-title">{props.name}</h2>
          </span>
      </div>
    );
}