import React from "react";
import "../css/budget.css"
import "../helpers.js"
import {euroFormatter} from "../helpers";

export default function BudgetAmounts(props) {
    return (
        <div className="budget-amounts-display">
            <div className="budget-amount">
                <span>Amount: {euroFormatter.format(props.amount)}</span>
            </div>
            <div className="budget-balance">
                <span>Balance: {euroFormatter.format(props.balance)}</span>
            </div>
        </div>

    );
}