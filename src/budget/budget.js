import React from "react";
import TransactionList from "../transaction/transactionList";
import BudgetTitle from "./budgetTitle";
import BudgetAmounts from "./budgetAmounts";

export default function Budget (props) {
    const budget = props.budget;
    return (
        <div className="budget">
            <BudgetTitle name={budget.name} />
            <BudgetAmounts amount={budget.amount} balance={budget.balance}/>
            <TransactionList transactions={budget.transactions}/>
        </div>
    );
}