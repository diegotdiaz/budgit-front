import {getData} from "./budgitApi";
const _budget_end_point = "budget?accountId=";
const _transaction_end_point = "transaction?budgetIds=";

export function getBudgetData(_accountId) {
    let budgetMapper =  function(response) {
        response.forEach(budgetDTO => budgetDTO.transactions = []);
    };
   return getData(_budget_end_point + _accountId, budgetMapper);
}

export function getTransactionsData(_budgetIds) {
    let budgetMapper =  function(response) {
        response.forEach(budgetDTO => budgetDTO.transactions = []);
    };
    return getData(_transaction_end_point + _budgetIds, budgetMapper);
}