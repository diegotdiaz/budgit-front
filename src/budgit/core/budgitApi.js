const _base_url = "http://localhost:8080/";

export  function getData(endpoint = '', mapper) {
    return fetch(_base_url + endpoint, {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        }
    }).catch(function (err) {
        console.log('Fetch problem: ' + err.message);
    });
}

export async function postData(endpoint = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(endpoint, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'no-cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

