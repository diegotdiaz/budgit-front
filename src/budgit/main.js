import Header from "../header/header";
import BudgetGrid from "../budget/budgetGrid";
import React from "react";
import {getBudgetData, getTransactionsData} from "./core/budgetApi";

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            budgets: []
        };
    }

    componentDidMount() {
        getBudgetData(1)
            .then(response => response.json())
            .then(this.mapBudgets());
    }

    mapBudgets() {
        return budgets => {
            let budgetIds = budgets.map(b => {
                return b.id
            }).join(',');
            console.log(budgets);
            getTransactionsData(budgetIds)
                .then(response => response.json())
                .then(this.mapTransactions());

            this.setState({
                budgets: budgets,
                isLoaded: true,
                error: null
            });
        };
    }

    mapTransactions() {
        return transactions => {
            let budgetsMap = new Map();
            this.state.budgets.slice().forEach(budget => budgetsMap.set(budget.id, budget));

            transactions.forEach(transaction => {
                budgetsMap.get(transaction.budgetId).transactions.push(transaction);
            });

            this.setState({
                budgets: Array.from(budgetsMap.values()),
                isLoaded: true,
                error: null
            });

        };
    }

    render() {
        const { error, isLoaded} = this.state;
        if (isLoaded) {
            return (
                <div>
                    <Header/>
                    <div className="spacer"/>
                    <BudgetGrid budgets={this.state.budgets}/>
                </div>
            );
        }
        if (error) {
            return (
                <div>
                    <p> Ups...something went wrong while loading Budgit </p>
                </div>
            );
        } else
        {
            return <div/>
        }
    }
}
export default Main;