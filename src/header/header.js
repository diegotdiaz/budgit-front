import React from "react";
import "../css/header.css"
import image from "../images/budgit_logo.png"

export default function Header() {
    return(
        <div className="header">
            <div className="logo">
                <a href="http://localhost:3000/">
                    <img alt="Budgit" src={image}/>
                </a>
            </div>
            <div >
            </div>
        </div>
    );
}