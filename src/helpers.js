export const euroFormatter = new Intl.NumberFormat('en-DE',
    { style: 'currency', currency: 'EUR',
        minimumFractionDigits: 2 });