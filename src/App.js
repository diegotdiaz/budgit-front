import React from 'react';
import './App.css';

import Main from "./budgit/main";

function App() {
  return <Main/>;
}

export default App;
